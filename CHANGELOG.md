[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module shariff
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Added possibility to disable and overwrite shariff settings for a single contentmeta record @cmoeke

### Fixed
- Bug at reading project domains to validate sharrif count requests @cmoeke
- Static function editDataSingular @cmoeke

### Changed
- Column sizes for fomantic @cmoeke

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- basic doc folder structure @cmoeke fafcms-core/37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core/38

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46
- Fixed bug when shariff count of an full url without protocol gets requested @cmoeke

### Removed
- Remove useless .browserslistrc

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-shariff/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-shariff/-/tree/v0.1.0-alpha
