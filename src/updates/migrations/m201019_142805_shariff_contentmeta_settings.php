<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff\updates\migrations;

use fafcms\fafcms\models\User;
use fafcms\shariff\models\Shariffcontentmeta;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\updater\base\Migration;

/**
 * Class m201019_142805_shariff_contentmeta_settings
 *
 * @package fafcms\shariff\updates\migrations
 */
class m201019_142805_shariff_contentmeta_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Shariffcontentmeta::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'contentmeta_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'disallow_shariff' => $this->tinyInteger(1)->unsigned()->null()->defaultValue(null),
            'custom_setting' => $this->tinyInteger(1)->unsigned()->null()->defaultValue(null),
            'show_count' => $this->tinyInteger(1)->unsigned()->null()->defaultValue(null),
            'backend_url' => $this->string(2048)->null()->defaultValue(null),
            'button_style' => $this->string(255)->null()->defaultValue(null),
            'flattr_category' => $this->string(255)->null()->defaultValue(null),
            'flattr_user' => $this->string(255)->null()->defaultValue(null),
            'info_url' => $this->string(2048)->null()->defaultValue(null),
            'info_display' => $this->tinyInteger(1)->unsigned()->null()->defaultValue(null),
            'lang' =>  $this->string(2)->null()->defaultValue(null),
            'mail_body' => $this->text()->null()->defaultValue(null),
            'mail_subject' => $this->string(2048)->null()->defaultValue(null),
            'mail_url' => $this->string(2048)->null()->defaultValue(null),
            'media_url' => $this->string(2048)->null()->defaultValue(null),
            'orientation' => $this->string(10)->null()->defaultValue(null),
            'referrer_track' => $this->string(255)->null()->defaultValue(null),
            'services' => $this->string(1024)->null()->defaultValue(null),
            'theme' => $this->string(20)->null()->defaultValue(null),
            'title' => $this->string(1024)->null()->defaultValue(null),
            'twitter_via' => $this->string(255)->null()->defaultValue(null),
            'url' => $this->string(2048)->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-shariffcontentmeta-contentmeta_id', Shariffcontentmeta::tableName(), ['contentmeta_id'], false);
        $this->createIndex('idx-shariffcontentmeta-created_by', Shariffcontentmeta::tableName(), ['created_by'], false);
        $this->createIndex('idx-shariffcontentmeta-updated_by', Shariffcontentmeta::tableName(), ['updated_by'], false);
        $this->createIndex('idx-shariffcontentmeta-activated_by', Shariffcontentmeta::tableName(), ['activated_by'], false);
        $this->createIndex('idx-shariffcontentmeta-deactivated_by', Shariffcontentmeta::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-shariffcontentmeta-deleted_by', Shariffcontentmeta::tableName(), ['deleted_by'], false);

        $this->addForeignKey('fk-shariffcontentmeta-contentmeta_id', Shariffcontentmeta::tableName(), 'contentmeta_id', Contentmeta::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-shariffcontentmeta-created_by', Shariffcontentmeta::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-shariffcontentmeta-updated_by', Shariffcontentmeta::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-shariffcontentmeta-activated_by', Shariffcontentmeta::tableName(), 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-shariffcontentmeta-deactivated_by', Shariffcontentmeta::tableName(), 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-shariffcontentmeta-deleted_by', Shariffcontentmeta::tableName(), 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropForeignKey('fk-shariffcontentmeta-contentmeta_id', Shariffcontentmeta::tableName());
        $this->dropForeignKey('fk-shariffcontentmeta-created_by', Shariffcontentmeta::tableName());
        $this->dropForeignKey('fk-shariffcontentmeta-updated_by', Shariffcontentmeta::tableName());
        $this->dropForeignKey('fk-shariffcontentmeta-activated_by', Shariffcontentmeta::tableName());
        $this->dropForeignKey('fk-shariffcontentmeta-deactivated_by', Shariffcontentmeta::tableName());
        $this->dropForeignKey('fk-shariffcontentmeta-deleted_by', Shariffcontentmeta::tableName());

        $this->dropIndex('idx-shariffcontentmeta-contentmeta_id', Shariffcontentmeta::tableName());
        $this->dropIndex('idx-shariffcontentmeta-created_by', Shariffcontentmeta::tableName());
        $this->dropIndex('idx-shariffcontentmeta-updated_by', Shariffcontentmeta::tableName());
        $this->dropIndex('idx-shariffcontentmeta-activated_by', Shariffcontentmeta::tableName());
        $this->dropIndex('idx-shariffcontentmeta-deactivated_by', Shariffcontentmeta::tableName());
        $this->dropIndex('idx-shariffcontentmeta-deleted_by', Shariffcontentmeta::tableName());

        $this->dropTable(Shariffcontentmeta::tableName());

        return true;
    }
}
