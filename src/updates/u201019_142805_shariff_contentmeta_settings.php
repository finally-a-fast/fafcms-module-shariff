<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff\updates;

use fafcms\shariff\updates\migrations\m201019_142805_shariff_contentmeta_settings;
use fafcms\updater\base\Update;

/**
 * Class u201019_142805_contentmeta_settings
 *
 * @package fafcms\shariff\updates
 */
class u201019_142805_shariff_contentmeta_settings extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        if (!$this->migrateUp(m201019_142805_shariff_contentmeta_settings::class)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        if (!$this->migrateDown(m201019_142805_shariff_contentmeta_settings::class)) {
            return false;
        }

        return true;
    }
}
