<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ShariffAsset
 * @package fafcms\shariff\assets
 */
class ShariffAsset extends AssetBundle
{
    public $sourcePath = '@npm/shariff/dist';

    public $css = [
        'shariff.complete.css',
    ];

    public $js = [
        'shariff.min.js',
    ];

    public $jsOptions = [
        'async' => true
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
