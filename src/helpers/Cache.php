<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff\helpers;

use fafcms\shariff\Bootstrap;
use Yii;
use Heise\Shariff\CacheInterface;

class Cache implements CacheInterface
{
    /**
     * @var StorageInterface
     */
    protected $cache;
    private $_ttl;

    /**
     * Cache constructor.
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        $this->_ttl = $configuration['ttl'];
    }

    /**
     * @param $key
     * @param $content
     */
    public function setItem($key, $content)
    {
        Yii::$app->cache->set(Bootstrap::$id.$key, $content, $this->_ttl);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getItem($key)
    {
        return Yii::$app->cache->get(Bootstrap::$id.$key);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function hasItem($key)
    {
        return Yii::$app->pluginCache->exists(Bootstrap::$id.$key);
    }
}
