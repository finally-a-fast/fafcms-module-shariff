<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\interfaces\ContentmetaInterface;
use fafcms\shariff\models\Shariffcontentmeta;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\Site;
use Yii;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\db\ActiveQuery;
use yii\i18n\PhpMessageSource;

/**
 * Class Bootstrap
 * @package fafcms\shariff
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-shariff';
    public static $tablePrefix = 'fafcms-shariff_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-shariff'])) {
            $app->i18n->translations['fafcms-shariff'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ .'/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    /**
     * @param Application  $app
     * @param PluginModule $module
     *
     * @return bool
     */
    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        $app->fafcms->addFrontendUrlRules(self::$id, [
            'shariff/count' => 'shariff/count',
        ], false);

        $app->view->addViewItems([
            'seo-tab' => [
                'class' => Tab::class,
                'contents' => [
                    'row-2' => [
                        'class' => Row::class,
                        'contents' => [
                            'column-1' => [
                                'class' => Column::class,
                                'settings' => [
                                    'm' => 8,
                                ],
                            ],
                            'column-2' => [
                                'class' => Column::class,
                                'settings' => [
                                    'm' => 8,
                                ],
                                'contents' => [
                                    'card-1' => [
                                        'class' => Card::class,
                                        'settings' => [
                                            'title' => [
                                                'fafcms-core',
                                                'Shariff settings',
                                            ],
                                            'icon' => 'share-variant-outline',
                                        ],
                                        'contents' => [
                                            'field-disallow_shariff' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.disallow_shariff',
                                                ],
                                            ],
                                            'field-custom_setting' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.custom_setting',
                                                ],
                                            ],
                                            'field-show_count' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.show_count',
                                                ],
                                            ],
                                            'field-backend_url' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.backend_url',
                                                ],
                                            ],
                                            'field-button_style' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.button_style',
                                                ],
                                            ],
                                            'field-flattr_category' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.flattr_category',
                                                ],
                                            ],
                                            'field-flattr_user' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.flattr_user',
                                                ],
                                            ],
                                            'field-info_url' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.info_url',
                                                ],
                                            ],
                                            'field-info_display' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.info_display',
                                                ],
                                            ],
                                            'field-lang' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.lang',
                                                ],
                                            ],
                                            'field-mail_body' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.mail_body',
                                                ],
                                            ],
                                            'field-mail_subject' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.mail_subject',
                                                ],
                                            ],
                                            'field-mail_url' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.mail_url',
                                                ],
                                            ],
                                            'field-media_url' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.media_url',
                                                ],
                                            ],
                                            'field-orientation' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.orientation',
                                                ],
                                            ],
                                            'field-referrer_track' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.referrer_track',
                                                ],
                                            ],
                                            'field-services' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.services',
                                                ],
                                            ],
                                            'field-theme' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.theme',
                                                ],
                                            ],
                                            'field-title' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.title',
                                                ],
                                            ],
                                            'field-twitter_via' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.twitter_via',
                                                ],
                                            ],
                                            'field-url' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'contentmeta.contentmetaShariffcontentmeta.url',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ], 'model\\edit\\', null, ContentmetaInterface::class);

        Yii::$app->injector->setModelExtensionRelation(Contentmeta::class, 'contentmetaShariffcontentmeta', function (): ActiveQuery {
            return $this->hasOne(Shariffcontentmeta::class, [
                'contentmeta_id' => 'id',
            ]);
        });

        return true;
    }
}
