<?php

namespace fafcms\shariff\models;

use \fafcms\shariff\abstracts\models\BaseShariffcontentmeta;

/**
 * This is the model class for table "{{%shariffcontentmeta}}".
 *
 * @package fafcms\shariff\models
 */
class Shariffcontentmeta extends BaseShariffcontentmeta
{
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();
        $fieldConfig['contentmeta_id']['options']['disabled'] = true;

        return $fieldConfig;
    }
}
