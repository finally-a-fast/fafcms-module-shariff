<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff\controllers;

use fafcms\shariff\Bootstrap;
use fafcms\shariff\helpers\Cache;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Heise\Shariff\Backend;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class ShariffController
 * @package fafcms\shariff\controllers
 */
class ShariffController extends Controller
{
    /***
     * @return array
     * @throws ForbiddenHttpException
     * @throws \Throwable
     */
    public function actionCount()
    {
        $active = Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('backend_active');

        if (!$active) {
            throw new ForbiddenHttpException('Shariff backend service has been disabled.');
        }

        $domains = [];
        $useProjectDomains = Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('use_project_domains');
        $additionalDomains = Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('additional_domains');

        if ($additionalDomains !== null && $additionalDomains !== '') {
            $domains = explode(',', $additionalDomains);
        }

        if ($useProjectDomains === 1) {
            $domains = array_merge($domains, ArrayHelper::getColumn(Yii::$app->fafcms->getCurrentProject()->projectDomains, 'domain'));
        }

        $shariff = new Backend([
            'domains' => $domains,
            'cache' => [
                'ttl' => Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('cache_ttl'),
            ],
            'cacheClass' => Cache::class,
            'services' => [
                'Facebook',
                'Reddit',
                'StumbleUpon',
                'Flattr',
                'Pinterest',
                'Xing',
                'AddThis',
                'Buffer',
                'Vk'
            ],
            'Facebook' => [
                'app_id' => Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('facebook_app_id'),
                'secret' => Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('facebook_secret')
            ]
        ]);

        $url = Yii::$app->request->getQueryParam('url', '');

        if (strpos($url, '//') === 0) {
            $url = 'http'.  (Yii::$app->fafcms->getCurrentProjectLanguage()->domain->force_https ? 's' : '') . ':' . $url;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $shariff->get($url);
    }
}
