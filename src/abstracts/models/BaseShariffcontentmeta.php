<?php

namespace fafcms\shariff\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use fafcms\sitemanager\models\Contentmeta;
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%shariffcontentmeta}}".
 *
 * @package fafcms\shariff\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int|null $contentmeta_id
 * @property int|null $disallow_shariff
 * @property int|null $custom_setting
 * @property int|null $show_count
 * @property string|null $backend_url
 * @property string|null $button_style
 * @property string|null $flattr_category
 * @property string|null $flattr_user
 * @property string|null $info_url
 * @property int|null $info_display
 * @property string|null $lang
 * @property string|null $mail_body
 * @property string|null $mail_subject
 * @property string|null $mail_url
 * @property string|null $media_url
 * @property string|null $orientation
 * @property string|null $referrer_track
 * @property string|null $services
 * @property string|null $theme
 * @property string|null $title
 * @property string|null $twitter_via
 * @property string|null $url
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property Contentmeta $contentmeta
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property User $updatedBy
 */
abstract class BaseShariffcontentmeta extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'shariffcontentmeta';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'shariffcontentmeta';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-shariff', 'Shariffcontentmetas');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-shariff', 'Shariffcontentmetum');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['title'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.title'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.title' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'contentmeta_id' => static function(...$params) {
                return Contentmeta::getOptions(...$params);
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['contentmeta_id'],
                'relationClassName' => Contentmeta::class,
            ],
            'disallow_shariff' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'custom_setting' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'show_count' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'backend_url' => [
                'type' => UrlInput::class,
            ],
            'button_style' => [
                'type' => TextInput::class,
            ],
            'flattr_category' => [
                'type' => TextInput::class,
            ],
            'flattr_user' => [
                'type' => TextInput::class,
            ],
            'info_url' => [
                'type' => UrlInput::class,
            ],
            'info_display' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'lang' => [
                'type' => TextInput::class,
            ],
            'mail_body' => [
                'type' => Textarea::class,
            ],
            'mail_subject' => [
                'type' => TextInput::class,
            ],
            'mail_url' => [
                'type' => UrlInput::class,
            ],
            'media_url' => [
                'type' => UrlInput::class,
            ],
            'orientation' => [
                'type' => TextInput::class,
            ],
            'referrer_track' => [
                'type' => TextInput::class,
            ],
            'services' => [
                'type' => TextInput::class,
            ],
            'theme' => [
                'type' => TextInput::class,
            ],
            'title' => [
                'type' => TextInput::class,
            ],
            'twitter_via' => [
                'type' => TextInput::class,
            ],
            'url' => [
                'type' => UrlInput::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['created_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['updated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['activated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deactivated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deleted_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'contentmeta_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'disallow_shariff',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'custom_setting',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'show_count',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'backend_url',
                        'sort' => 7,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'button_style',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'flattr_category',
                        'sort' => 9,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'flattr_user',
                        'sort' => 10,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'info_url',
                        'sort' => 11,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'info_display',
                        'sort' => 12,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'lang',
                        'sort' => 13,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mail_body',
                        'sort' => 14,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mail_subject',
                        'sort' => 15,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mail_url',
                        'sort' => 16,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'media_url',
                        'sort' => 17,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'orientation',
                        'sort' => 18,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'referrer_track',
                        'sort' => 19,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'services',
                        'sort' => 20,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'theme',
                        'sort' => 21,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'twitter_via',
                        'sort' => 22,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'url',
                        'sort' => 23,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'contentmeta_id',
                                                    ],
                                                ],
                                                'field-disallow_shariff' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'disallow_shariff',
                                                    ],
                                                ],
                                                'field-custom_setting' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'custom_setting',
                                                    ],
                                                ],
                                                'field-show_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'show_count',
                                                    ],
                                                ],
                                                'field-backend_url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'backend_url',
                                                    ],
                                                ],
                                                'field-button_style' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'button_style',
                                                    ],
                                                ],
                                                'field-flattr_category' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'flattr_category',
                                                    ],
                                                ],
                                                'field-flattr_user' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'flattr_user',
                                                    ],
                                                ],
                                                'field-info_url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'info_url',
                                                    ],
                                                ],
                                                'field-info_display' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'info_display',
                                                    ],
                                                ],
                                                'field-lang' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'lang',
                                                    ],
                                                ],
                                                'field-mail_body' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mail_body',
                                                    ],
                                                ],
                                                'field-mail_subject' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mail_subject',
                                                    ],
                                                ],
                                                'field-mail_url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mail_url',
                                                    ],
                                                ],
                                                'field-media_url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'media_url',
                                                    ],
                                                ],
                                                'field-orientation' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'orientation',
                                                    ],
                                                ],
                                                'field-referrer_track' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'referrer_track',
                                                    ],
                                                ],
                                                'field-services' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'services',
                                                    ],
                                                ],
                                                'field-theme' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'theme',
                                                    ],
                                                ],
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-twitter_via' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'twitter_via',
                                                    ],
                                                ],
                                                'field-url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'url',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%shariffcontentmeta}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-contentmeta_id' => ['contentmeta_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'boolean-disallow_shariff' => ['disallow_shariff', 'boolean'],
            'boolean-custom_setting' => ['custom_setting', 'boolean'],
            'boolean-show_count' => ['show_count', 'boolean'],
            'boolean-info_display' => ['info_display', 'boolean'],
            'string-mail_body' => ['mail_body', 'string'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-backend_url' => ['backend_url', 'string', 'max' => 2048],
            'string-info_url' => ['info_url', 'string', 'max' => 2048],
            'string-mail_subject' => ['mail_subject', 'string', 'max' => 2048],
            'string-mail_url' => ['mail_url', 'string', 'max' => 2048],
            'string-media_url' => ['media_url', 'string', 'max' => 2048],
            'string-url' => ['url', 'string', 'max' => 2048],
            'string-button_style' => ['button_style', 'string', 'max' => 255],
            'string-flattr_category' => ['flattr_category', 'string', 'max' => 255],
            'string-flattr_user' => ['flattr_user', 'string', 'max' => 255],
            'string-referrer_track' => ['referrer_track', 'string', 'max' => 255],
            'string-twitter_via' => ['twitter_via', 'string', 'max' => 255],
            'string-lang' => ['lang', 'string', 'max' => 2],
            'string-orientation' => ['orientation', 'string', 'max' => 10],
            'string-services' => ['services', 'string', 'max' => 1024],
            'string-title' => ['title', 'string', 'max' => 1024],
            'string-theme' => ['theme', 'string', 'max' => 20],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-contentmeta_id' => [['contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['contentmeta_id' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-shariff', 'ID'),
            'contentmeta_id' => Yii::t('fafcms-shariff', 'Contentmeta ID'),
            'disallow_shariff' => Yii::t('fafcms-shariff', 'Disallow Shariff'),
            'custom_setting' => Yii::t('fafcms-shariff', 'Custom Setting'),
            'show_count' => Yii::t('fafcms-shariff', 'Show Count'),
            'backend_url' => Yii::t('fafcms-shariff', 'Backend Url'),
            'button_style' => Yii::t('fafcms-shariff', 'Button Style'),
            'flattr_category' => Yii::t('fafcms-shariff', 'Flattr Category'),
            'flattr_user' => Yii::t('fafcms-shariff', 'Flattr User'),
            'info_url' => Yii::t('fafcms-shariff', 'Info Url'),
            'info_display' => Yii::t('fafcms-shariff', 'Info Display'),
            'lang' => Yii::t('fafcms-shariff', 'Lang'),
            'mail_body' => Yii::t('fafcms-shariff', 'Mail Body'),
            'mail_subject' => Yii::t('fafcms-shariff', 'Mail Subject'),
            'mail_url' => Yii::t('fafcms-shariff', 'Mail Url'),
            'media_url' => Yii::t('fafcms-shariff', 'Media Url'),
            'orientation' => Yii::t('fafcms-shariff', 'Orientation'),
            'referrer_track' => Yii::t('fafcms-shariff', 'Referrer Track'),
            'services' => Yii::t('fafcms-shariff', 'Services'),
            'theme' => Yii::t('fafcms-shariff', 'Theme'),
            'title' => Yii::t('fafcms-shariff', 'Title'),
            'twitter_via' => Yii::t('fafcms-shariff', 'Twitter Via'),
            'url' => Yii::t('fafcms-shariff', 'Url'),
            'created_by' => Yii::t('fafcms-shariff', 'Created By'),
            'updated_by' => Yii::t('fafcms-shariff', 'Updated By'),
            'activated_by' => Yii::t('fafcms-shariff', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-shariff', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-shariff', 'Deleted By'),
            'created_at' => Yii::t('fafcms-shariff', 'Created At'),
            'updated_at' => Yii::t('fafcms-shariff', 'Updated At'),
            'activated_at' => Yii::t('fafcms-shariff', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-shariff', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-shariff', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[Contentmeta]].
     *
     * @return ActiveQuery
     */
    public function getContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
