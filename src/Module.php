<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff;

use fafcms\fafcms\inputs\Chips;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Module
 * @package fafcms\shariff
 */
class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [['backend_active', 'use_project_domains'], 'boolean'],
            [['additional_domains'], 'string'],
            [['cache_ttl'], 'integer'],
            [['facebook_app_id', 'facebook_secret'], 'string'],
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'backend_active',
                'label' => Yii::t('fafcms-shariff', 'Backend active'),
                'defaultValue' => true,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'cache_ttl',
                'label' => Yii::t('fafcms-shariff', 'Cache ttl'),
                'description' => Yii::t('fafcms-core', 'Time that the counts are cached (in seconds, one hour = 3600 seconds, one day = 86400 seconds)'),
                'inputType' => NumberInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'defaultValue' => 86400
            ]),
            new PluginSetting($this, [
                'name' => 'use_project_domains',
                'label' => Yii::t('fafcms-shariff', 'Use default project domains'),
                'description' => Yii::t('fafcms-core', 'Use default project domains for which share counts may be requested for.'),
                'defaultValue' => true,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'additional_domains',
                'label' => Yii::t('fafcms-shariff', 'Additional domains'),
                'description' => Yii::t('fafcms-core', 'Additional domains for which share counts may be requested for.'),
                'inputType' => Chips::class,
            ]),
            new PluginSetting($this, [
                'name' => 'facebook_app_id',
                'label' => Yii::t('fafcms-shariff', 'The id of your facebook application'),
                'inputType' => TextInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'facebook_secret',
                'label' => Yii::t('fafcms-shariff', 'The client secret of your facebook application'),
                'inputType' => TextInput::class,
            ]),
        ];
    }
}
