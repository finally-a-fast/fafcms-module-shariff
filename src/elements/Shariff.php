<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff\elements;

use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use fafcms\shariff\assets\ShariffAsset;
use fafcms\shariff\models\Shariffcontentmeta;
use yii\helpers\Url;
use Yiisoft\Validator\Rule\Boolean;
use Yiisoft\Validator\Rule\InRange;
use Yii;

/**
 * Class Shariff
 *
 * @package fafcms\shariff\elements
 * @property array{function: string, params: array} $data
 */
class Shariff extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'shariff';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return 'Displays shariff buttons.';
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
                'name' => 'show-count',
                'label' => 'Show count',
                'defaultValue' => 1,
                'rules' => [
                    new Boolean(),
                ],
            ]),
            new ElementSetting([
                'name' => 'backend-url',
                'label' => 'Backend url',
            ]),
            new ElementSetting([
                'name' => 'button-style',
                'label' => 'Button style',
                'rules' => [
                    new InRange(['standard', 'icon', 'icon-count']),
                ],
            ]),
            new ElementSetting([
                'name' => 'flattr-category',
                'label' => 'Flattr category',
            ]),
            new ElementSetting([
                'name' => 'flattr-user',
                'label' => 'Flattr user',
            ]),
            new ElementSetting([
                'name' => 'info-url',
                'label' => 'Info url',
            ]),
            new ElementSetting([
                'name' => 'info-display',
                'label' => 'Info display',
                'rules' => [
                    new InRange([null, 'blank', 'popup', 'self']),
                ],
            ]),
            new ElementSetting([
                'name' => 'lang',
                'label' => 'Lang',
                'rules' => [
                    new InRange([null, 'bg', 'cs', 'da', 'de', 'en', 'es', 'fi', 'fr', 'hr', 'hu', 'it', 'ja', 'ko', 'nl', 'no', 'pl', 'pt', 'ro', 'ru', 'sk', 'sl', 'sr', 'sv', 'tr', 'zh']),
                ],
            ]),
            new ElementSetting([
                'name' => 'mail-body',
                'label' => 'Mail body',
            ]),
            new ElementSetting([
                'name' => 'mail-subject',
                'label' => 'Mail subject',
            ]),
            new ElementSetting([
                'name' => 'mail-url',
                'label' => 'Mail url',
            ]),
            new ElementSetting([
                'name' => 'media-url',
                'label' => 'Media url',
            ]),
            new ElementSetting([
                'name' => 'orientation',
                'label' => 'Orientation',
                'rules' => [
                    new InRange([null, 'vertical', 'horizontal']),
                ],
            ]),
            new ElementSetting([
                'name' => 'referrer-track',
                'label' => 'Referrer track',
            ]),
            new ElementSetting([
                'name' => 'services',
                'label' => 'Services',
                //twitter, facebook, linkedin, pinterest, xing, whatsapp, mail, info, addthis, tumblr, flattr, diaspora, reddit, stumbleupon, threema, weibo, tencent-weibo, qzone, print, telegram, vk, flipboard, pocket, buffer
            ]),
            new ElementSetting([
                'name' => 'theme',
                'label' => 'Theme',
                'rules' => [
                    new InRange([null, 'standard', 'grey', 'white']),
                ],
            ]),
            new ElementSetting([
                'name' => 'title',
                'label' => 'Title',
                'element' => ShariffTitle::class,
            ]),
            new ElementSetting([
                'name' => 'twitter-via',
                'label' => 'Twitter via',
            ]),
            new ElementSetting([
                'name' => 'url',
                'label' => 'Url',
                'element' => ShariffUrl::class,
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        /**
         * @var $shariffcontentmeta Shariffcontentmeta
         */
        $shariffcontentmeta = $this->getParser()->getAttributeData('currentContentmeta.contentmetaShariffcontentmeta');

        if (($shariffcontentmeta->disallow_shariff ?? 0) !== 1) {
            if ($shariffcontentmeta !== null && $shariffcontentmeta->custom_setting === 1) {
                if ($shariffcontentmeta->show_count !== null) {
                    $this->data['show-count'] = $shariffcontentmeta->show_count;
                }

                if ($shariffcontentmeta->backend_url !== null && $shariffcontentmeta->backend_url !== '') {
                    $this->data['backend-url'] = $shariffcontentmeta->backend_url;
                }

                if ($shariffcontentmeta->button_style !== null && $shariffcontentmeta->button_style !== '') {
                    $this->data['button-style'] = $shariffcontentmeta->button_style;
                }

                if ($shariffcontentmeta->flattr_category !== null && $shariffcontentmeta->flattr_category !== '') {
                    $this->data['flattr-category'] = $shariffcontentmeta->flattr_category;
                }

                if ($shariffcontentmeta->flattr_user !== null && $shariffcontentmeta->flattr_user !== '') {
                    $this->data['flattr-user'] = $shariffcontentmeta->flattr_user;
                }

                if ($shariffcontentmeta->info_url !== null && $shariffcontentmeta->info_url !== '') {
                    $this->data['info-url'] = $shariffcontentmeta->info_url;
                }

                if ($shariffcontentmeta->info_display !== null) {
                    $this->data['info-display'] = $shariffcontentmeta->info_display;
                }

                if ($shariffcontentmeta->lang !== null && $shariffcontentmeta->lang !== '') {
                    $this->data['lang'] = $shariffcontentmeta->lang;
                }

                if ($shariffcontentmeta->mail_body !== null && $shariffcontentmeta->mail_body !== '') {
                    $this->data['mail-body'] = $shariffcontentmeta->mail_body;
                }

                if ($shariffcontentmeta->mail_subject !== null && $shariffcontentmeta->mail_subject !== '') {
                    $this->data['mail-subject'] = $shariffcontentmeta->mail_subject;
                }

                if ($shariffcontentmeta->mail_url !== null && $shariffcontentmeta->mail_url !== '') {
                    $this->data['mail-url'] = $shariffcontentmeta->mail_url;
                }

                if ($shariffcontentmeta->media_url !== null && $shariffcontentmeta->media_url !== '') {
                    $this->data['media-url'] = $shariffcontentmeta->media_url;
                }

                if ($shariffcontentmeta->orientation !== null && $shariffcontentmeta->orientation !== '') {
                    $this->data['orientation'] = $shariffcontentmeta->orientation;
                }

                if ($shariffcontentmeta->referrer_track !== null && $shariffcontentmeta->referrer_track !== '') {
                    $this->data['referrer-track'] = $shariffcontentmeta->referrer_track;
                }

                if ($shariffcontentmeta->services !== null && $shariffcontentmeta->services !== '') {
                    $this->data['services'] = $shariffcontentmeta->services;
                }

                if ($shariffcontentmeta->theme !== null && $shariffcontentmeta->theme !== '') {
                    $this->data['theme'] = $shariffcontentmeta->theme;
                }

                if ($shariffcontentmeta->title !== null && $shariffcontentmeta->title !== '') {
                    $this->data['title'] = $shariffcontentmeta->title;
                }

                if ($shariffcontentmeta->twitter_via !== null && $shariffcontentmeta->twitter_via !== '') {
                    $this->data['twitter-via'] = $shariffcontentmeta->twitter_via;
                }

                if ($shariffcontentmeta->url !== null && $shariffcontentmeta->url !== '') {
                    $this->data['url'] = $shariffcontentmeta->url;
                }
            }

            $options = [];

            if ($this->data['show-count'] === 1) {
                if ($this->data['backend-url'] !== null) {
                    $options['data-backend-url'] = $this->data['backend-url'];
                } else {
                    $options['data-backend-url'] = Url::to(['shariff/count']);
                }
            }

            if ($this->data['button-style'] !== null) {
                $options['data-button-style'] = $this->data['button-style'];
            }

            if ($this->data['flattr-category'] !== null) {
                $options['data-flattr-category'] = $this->data['flattr-category'];
            }

            if ($this->data['flattr-user'] !== null) {
                $options['data-flattr-user'] = $this->data['flattr-user'];
            }

            if ($this->data['info-url'] !== null) {
                $options['data-info-url'] = $this->data['info-url'];
            }

            if ($this->data['info-display'] !== null) {
                $options['data-info-display'] = $this->data['info-display'];
            }

            if ($this->data['lang'] !== null) {
                $options['data-lang'] = $this->data['lang'];
            }

            if ($this->data['mail-body'] !== null) {
                $options['data-mail-body'] = $this->data['mail-body'];
            }

            if ($this->data['mail-subject'] !== null) {
                $options['data-mail-subject'] = $this->data['mail-subject'];
            }

            if ($this->data['mail-url'] !== null) {
                $options['data-mail-url'] = $this->data['mail-url'];
            }

            if ($this->data['media-url'] !== null) {
                $options['data-media-url'] = $this->data['media-url'];
            }

            if ($this->data['orientation'] !== null) {
                $options['data-orientation'] = $this->data['orientation'];
            }

            if ($this->data['referrer-track'] !== null) {
                $options['data-referrer-track'] = $this->data['referrer-track'];
            }

            if ($this->data['services'] !== null) {
                $services = explode(',', $this->data['services']);
                $services = array_map('trim', $services);
                $options['data-services'] = $services;
            }

            if ($this->data['theme'] !== null) {
                $options['data-theme'] = $this->data['theme'];
            }

            if ($this->data['title'] !== null) {
                $options['data-title'] = $this->getParser()->fullTrim($this->data['title']);
            }

            if ($this->data['twitter-via'] !== null) {
                $options['data-twitter-via'] = $this->data['twitter-via'];
            }

            if ($this->data['url'] !== null) {
                $options['data-url'] = $this->getParser()->fullTrim($this->data['url']);
            }

            if (!isset($options['class'])) {
                $options['class'] = '';
            }

            $options['class'] .= ($options['class'] === '' ? '' : ' ') . 'shariff';

            ShariffAsset::register(Yii::$app->view);

            return $this->parser->htmlTag('div', '', $options);
        }

        return '';
    }
}
