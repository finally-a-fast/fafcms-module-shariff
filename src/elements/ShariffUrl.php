<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-shariff/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-shariff
 * @see https://www.finally-a-fast.com/packages/fafcms-module-shariff/docs Documentation of fafcms-module-shariff
 * @since File available since Release 1.0.0
 */

namespace fafcms\shariff\elements;

use Faf\TemplateEngine\Helpers\ParserElement;

/**
 * Class ShariffUrl
 *
 * @package fafcms\shariff\elements
 * @property array{function: string, params: array} $data
 */
class ShariffUrl extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'shariff-url';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return 'Url';
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return [Shariff::class];
    }

    /**
     * {@inheritdoc}
     * @return array<int|string, mixed>|string|int|float|bool|object|null
     */
    public function run()
    {
        return $this->content;
    }
}
